# Maintainer: George Rawlinson <grawlinson@archlinux.org>

pkgname=drone-runner-ssh
pkgver=1.0.1
pkgrel=5
pkgdesc='Drone runner that executes a pipeline on a remote machine'
arch=('x86_64')
url='https://github.com/drone-runners/drone-runner-ssh'
license=('LicenseRef-Polyform-Small-Business-1.0.0' 'LicenseRef-Polyform-Free-Trial-1.0.0' 'LicenseRef-Polyform-Noncommercial-1.0.0')
depends=('glibc')
makedepends=('git' 'go')
optdepends=('drone: for a local Drone instance')
options=('!lto')
source=(
  "$pkgname::git+$url#tag=v$pkgver"
  'systemd.service'
  'sysusers.conf'
  'tmpfiles.conf'
)
sha512sums=('a73baf00f44eb6a441f52e543bb9d0bd6f3d5436e647fcfd4069469c149e67f81544b8fce765ef1a555b2b72f7e855118f9587d3f6c0da96708f935a4677f6ea'
            '6eaa30699fbbaeebaa824add5c1c921e74042d200ae699c393914f3c5f90ae9cdafbd6bbe9d5d555f4914ad9c37ccc897bb0ba65c87dc5fa14b975df0f3da8ab'
            '3c5569fe7214b8a66bc49bd952f21530db5af192b81398b57a629faf7430e53a914648a0882947faa1c8258adb046472813b09ec9e607ee45f6d06d081c89009'
            '239c3eda7cf7ffcac5df80699ec231f2797f4f35b18b26d570303a7597755d1e4958152139ddb3bb5268fe711a1fb055aa97b8d054af13653b0d5f2e35dfb537')
b2sums=('b6493ebe5a5f44f6ed7737568113762fc882e01cde68ba5cd65f768febcbf057cb0220a16d6e8b809585f76f1a9917379d4dbc3ba978445a253968ada6e551eb'
        '90d868e9f6932eedc814473e160c661da7cf06a3cf1adf0709b462e388130f3f149120cf36dc6bb9f6bffbca8fce7c8624aa8a0b2511ea64d9b72dc950eea746'
        '52cd1d779a779684d3e62f44e677bbdfad9f38152c80c6251bf6d1ea6600d263a9743da6179d8f8d7e6c9f3413c0f2ea524de078ada625176335cc908acdca87'
        'f2742d5c60a3b68ecf7dab2d602b442bafcc619d838fcc74f5f186113778719797f9fa3975643c0925def2acf282c09120c186abc90b1ee5a34a7796b38f00fd')

prepare() {
  cd "$pkgname"

  # create directory for build output
  mkdir -p build

  # download dependencies
  go mod download
}

build() {
  cd "$pkgname"

  # set Go flags
  export CGO_CPPFLAGS="${CPPFLAGS}"
  export CGO_CFLAGS="${CFLAGS}"
  export CGO_CXXFLAGS="${CXXFLAGS}"

  go build -v \
    -trimpath \
    -buildmode=pie \
    -mod=readonly\
    -modcacherw \
    -ldflags "-linkmode external -extldflags \"${LDFLAGS}\"" \
    -o build \
    .
}

check() {
  cd "$pkgname"
  go test -v ./...
}

package() {
  # systemd integration
  install -vDm644 systemd.service "$pkgdir/usr/lib/systemd/system/$pkgname.service"
  install -vDm644 sysusers.conf "$pkgdir/usr/lib/sysusers.d/$pkgname.conf"
  install -vDm644 tmpfiles.conf "$pkgdir/usr/lib/tmpfiles.d/$pkgname.conf"

  cd "$pkgname"

  # binary
  install -vDm755 -t "$pkgdir/usr/bin" build/*

  # licenses
  install -vDm644 -t "$pkgdir/usr/share/licenses/$pkgname" licenses/* LICENSE.md

  # documentation
  install -vDm644 -t "$pkgdir/usr/share/doc/$pkgname" README.md CHANGELOG.md
}
